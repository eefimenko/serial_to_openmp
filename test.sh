echo "Static schedule, chunk size = 1"
echo "==============================="
export OMP_SCHEDULE=static,1
echo "1 thread"
export OMP_NUM_THREADS=1
./transport
echo "2 threads"
export OMP_NUM_THREADS=2
./transport
echo "4 threads"
export OMP_NUM_THREADS=4
./transport
echo "8 threads"
export OMP_NUM_THREADS=8
./transport

echo "\n\n\n"
echo "Static schedule, chunk size = 2"
echo "==============================="

export OMP_SCHEDULE=static,2
echo "1 thread"
export OMP_NUM_THREADS=1
./transport
echo "2 threads"
export OMP_NUM_THREADS=2
./transport
echo "4 threads"
export OMP_NUM_THREADS=4
./transport
echo "8 threads"
export OMP_NUM_THREADS=8
./transport

echo "\n\n\n"
echo "Static schedule, chunk size = 10"
echo "==============================="

export OMP_SCHEDULE=static,10
echo "1 thread"
export OMP_NUM_THREADS=1
./transport
echo "2 threads"
export OMP_NUM_THREADS=2
./transport
echo "4 threads"
export OMP_NUM_THREADS=4
./transport
echo "8 threads"
export OMP_NUM_THREADS=8
./transport

echo "\n\n\n"
echo "Dynamic schedule, chunk size = 1"
echo "==============================="

export OMP_SCHEDULE=dynamic,1
echo "1 thread"
export OMP_NUM_THREADS=1
./transport
echo "2 threads"
export OMP_NUM_THREADS=2
./transport
echo "4 threads"
export OMP_NUM_THREADS=4
./transport
echo "8 threads"
export OMP_NUM_THREADS=8
./transport

echo "\n\n\n"
echo "Dynamic schedule, chunk size = 2"
echo "==============================="

export OMP_SCHEDULE=dynamic,2
echo "1 thread"
export OMP_NUM_THREADS=1
./transport
echo "2 threads"
export OMP_NUM_THREADS=2
./transport
echo "4 threads"
export OMP_NUM_THREADS=4
./transport
echo "8 threads"
export OMP_NUM_THREADS=8
./transport

echo "\n\n\n"
echo "Dynamic schedule, chunk size = 10"
echo "==============================="

export OMP_SCHEDULE=dynamic,10
echo "1 thread"
export OMP_NUM_THREADS=1
./transport
echo "2 threads"
export OMP_NUM_THREADS=2
./transport
echo "4 threads"
export OMP_NUM_THREADS=4
./transport
echo "8 threads"
export OMP_NUM_THREADS=8
./transport
